# TravelIngest

L'idée de TravelIngest est d'ingester les média (photos, vidéo), lorsqu'on branche un périphérique de média  (réflex, smartphone, gopro, ...), de les classer par type et dates.

les fonctionnalités seront les suivantes :
Transfert
- Choix du dossier de destination
- Nom du projet
- création de dossier par jour AAAA-MM-JJ
- Sélection des média selon période : après __date__ et  avant __date__

Metadonnees
- renommage des medat en fonction de la date
- gestion d'un décalage Horaire
- geotag des photos si fichiers GPX trouvé
- ajout metadonnée manuelle : _______________
- reconnaissance de type de photo (paysage, portrait, plantes, animaux...)

Transformation
- Création d'image préparée pour le web en ________x________
- Rotation auto en fonction des données exif ?
- Imagettes depuis video
- génération auto de timelaspe ?



